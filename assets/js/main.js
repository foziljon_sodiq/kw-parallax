/**
 * Main Menu
 */
$(function () {
  let MenuStatus = false;

  $(".ham-menu").click(function () {
    $(".main-menu").removeClass("no-active");
    $(".ham-menu").addClass("d-none");
    $(".ham-menu-x").removeClass("d-none");
  });

  $(".ham-menu-x").click(function () {
    $(".main-menu").addClass("no-active");
    $(".ham-menu").removeClass("d-none");
    $(".ham-menu-x").addClass("d-none");
  });

  $(".menu-switcher").click(function () {
    if (!MenuStatus) {
      $(".main-menu").removeClass("no-active");
      $(".ham-menu").addClass("d-none");
      $(".ham-menu-x").removeClass("d-none");
      MenuStatus = true;
    } else {
      $(".main-menu").addClass("no-active");
      $(".ham-menu").removeClass("d-none");
      $(".ham-menu-x").addClass("d-none");
      MenuStatus = false;
    }
  });
});

/**
 * Slider
 * substr("testers", -1);
 */
$(function () {
  let idActive = 1;
  $("label").click(function (e) {
    idClick = Number((IdClick = $(this).attr("t")));

    $(`.slide-${idActive}`).removeClass("act-slid");
    $(`.slide-${idClick}`).addClass("act-slid");

    if (idClick == 1) {
      $(".section").removeClass(`section-${idActive}`);
      $(".section").addClass(`section-${idClick}`);
      idActive = 1;
    }

    if (idClick == 2) {
      $(".section").removeClass(`section-${idActive}`);
      $(".section").addClass(`section-${idClick}`);
      idActive = 2;
    }

    if (idClick == 3) {
      $(".section").removeClass(`section-${idActive}`);
      $(".section").addClass(`section-${idClick}`);
      idActive = 3;
    }

    if (idClick == 4) {
      $(".section").removeClass(`section-${idActive}`);
      $(".section").addClass(`section-${idClick}`);
      idActive = 4;
    }
 
  });
});

$(function () {
  let Active = 2;

  $(".sw").click(function (e) {
    idClick = Number((IdClick = $(this).attr("s")));

    $(`.da-1`).removeClass("n-active");
    $(`.da-2`).removeClass("n-active");
    $(`.ad-1`).removeClass("pr");
    $(`.ad-2`).removeClass("pr");

    if (Active == 1) {
      Active = 2;
    } else {
      Active = 1;
    }

    $(`.da-${Active}`).addClass("n-active");
    $(`.ad-${Active}`).addClass("pr");
  });
});

$(function () {
  let idActive = 1;
  $(".short-water").click(function (e) {
    // idClick = Number((IdClick = $(this).attr("t")));
    let title = $("#short-tea") 
    let a = title[0].textContent
    console.log(title[0].textContent);
    title[0].textContent = e.target.textContent
    // $(".short-tea")[0].textContent = e.target.textContent
    e.target.textContent = a
    console.log(e.target.textContent);










    // $(`.slide-${idActive}`).removeClass("act-slid");
    // $(`.slide-${idClick}`).addClass("act-slid");

    // if (idClick == 1) {
    //   $(".section").removeClass(`section-${idActive}`);
    //   $(".section").addClass(`section-${idClick}`);
    //   idActive = 1;
    // }

    // if (idClick == 2) {
    //   $(".section").removeClass(`section-${idActive}`);
    //   $(".section").addClass(`section-${idClick}`);
    //   idActive = 2;
    // }

    // if (idClick == 3) {
    //   $(".section").removeClass(`section-${idActive}`);
    //   $(".section").addClass(`section-${idClick}`);
    //   idActive = 3;
    // }

    // if (idClick == 4) {
    //   $(".section").removeClass(`section-${idActive}`);
    //   $(".section").addClass(`section-${idClick}`);
    //   idActive = 4;
    // }

    // console.log(idClick);
  });
});

// parallax
let lastX = 0;
addEventListener("mousemove", (event) => {
  // console.log(event.pageX);
  let parallax = document.querySelector(".paralax");
  if (lastX != event.pageX) {
    parallax.querySelectorAll("img").forEach((layer) => {
      const speed = layer.getAttribute("data-speed");
      //
      const x = (window.innerWidth - event.pageX * speed) / 100;
      //
      let ll = layer.style.left;
      // console.log(Number(ll.slice(0, -2)), "style");

      // console.log(Number(ll.slice(0, -2)) + x, "x");
      let res = x;
      layer.style.left = `${res}px`;
    });
  }

  lastX = event.pageX;
});
